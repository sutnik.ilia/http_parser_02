package pars;

import org.apache.hc.client5.http.fluent.Request;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        String contentNews = Request.post("https://www.opennet.ru/opennews/mini.shtml")
                .execute()
                .returnContent()
                .asString();
        String[] words = contentNews.split(" ");
        int count = 0;
        for (String word : words) {
            if (word.contains("tdate") && word.contains("31.08.2021")) {
                count++;
            }
        }
        System.out.println(count);
    }
}
